package cn.paas.controller;

import cn.paas.constant.CommonConst;
import cn.paas.pojo.SysUser;
import cn.paas.pojo.UserActive;
import cn.paas.service.interfaces.ISysUserService;
import cn.paas.utils.BuildRandom;
import cn.paas.utils.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * paas统一入口
 *
 * 例如: 118.31.5.5:8080/paas/loginEnroll
 *      118.31.5.5:8080/paas/openMember
 *
 */
@Controller
@RequestMapping("/paas")
public class PaaSController {

    @Resource
    private ISysUserService userService;

    @RequestMapping("/login")
    @ResponseBody
    public Response login(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        String mobile = request.getParameter("mobile");
//        String loginPass = request.getParameter("password");
//
//        if (userService.loginUser(mobile, loginPass) == 1) {
//            // 统计登录次数
//            UserActive userActive = new UserActive();
//            userActive.setMobile(mobile);
//            userActive.setCount(CommonConst.Count.one);
//
//            userService.userCount(userActive);
//
//        } else {
//            SysUser sysUser = new SysUser();
//            sysUser.setLoginName(String.format("游客%d", BuildRandom.eight()));
//            sysUser.setMobile(mobile);
//            sysUser.setLoginPass(loginPass);
//
//            userService.registerUser(sysUser);
//        }

        return new Response().success();
    }

    @RequestMapping("/loginEnroll")
    public String loginEnroll(HttpServletRequest request) throws Exception {
        String mobile = request.getParameter("mobile");
        String loginPass = request.getParameter("password");
//        request.getSession().setAttribute("unique",mobile);

        if (userService.loginUser(mobile, loginPass) == 1) {
            // 统计登录次数
            UserActive userActive = new UserActive();
            userActive.setMobile(mobile);
            userActive.setCount(CommonConst.Count.one);

            userService.userCount(userActive);

        } else {
            SysUser sysUser = new SysUser();
            sysUser.setLoginName(String.format("游客%d", BuildRandom.eight()));
            sysUser.setMobile(mobile);
            sysUser.setLoginPass(loginPass);

            userService.registerUser(sysUser);
        }

        return "index";
    }

    @RequestMapping("/openMember")
    public String openMember(HttpServletRequest request) throws Exception {
        String mobile = request.getParameter("mobile");

        userService.openMember(mobile);

        return "person";
    }
}