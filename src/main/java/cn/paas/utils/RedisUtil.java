package cn.paas.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * redis 工具类
 */
public class RedisUtil {

    private static JedisPool jedisPool = null;

    static {
        try {
            JedisPoolConfig config = new JedisPoolConfig();
            int MAX_ACTIVE = 1024;
            config.setMaxTotal(MAX_ACTIVE);
            int MAX_IDLE = 200;
            config.setMaxIdle(MAX_IDLE);
            int MAX_WAIT = 10000;
            config.setMaxWaitMillis(MAX_WAIT);
            boolean TEST_ON_BORROW = true;
            config.setTestOnBorrow(true);
            String ADDR = "118.31.5.5";
            int PORT = 6379;
            String AUTH = "lshc";
            int TIMEOUT = 10000;
            jedisPool = new JedisPool(config, ADDR, PORT, TIMEOUT,AUTH);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取Jedis实例
     *
     * @return edis实例
     */
    public synchronized static Jedis getJedis() {
        try {
            if (jedisPool != null) return jedisPool.getResource();
            else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 释放jedis资源
     *
     * @param jedis 释放jedis
     */
    public static void returnResource(final Jedis jedis) {
        if (jedis != null) {
            jedisPool.returnResource(jedis);
        }
    }
}
