package cn.paas.utils;


import cn.paas.service.interfaces.ISysUserService;
import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@ContextConfiguration(locations = {"classpath:spring-mybatis.xml"})
//@ContextConfiguration(locations = {"classpath:*"})
@RunWith(SpringJUnit4ClassRunner.class)     //表示继承了SpringJUnit4ClassRunner类
@Transactional
@TransactionConfiguration(defaultRollback = false)
public class TestDemo {

    private static Logger logger = Logger.getLogger(TestDemo.class);

    @Resource
    private ISysUserService userService = null;

    @org.junit.Test
    public void defineTest() throws Exception {
        int i = userService.loginUser("17615835607", "123321");
        System.out.println(i);
    }

}
