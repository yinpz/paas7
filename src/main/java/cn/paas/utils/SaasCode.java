package cn.paas.utils;

/**
 * PAAS服务通用错误代码
 * 注意：对于模块个性化的错误代码，请定义到模块工程里，不要在这里定义
 */
public interface SaasCode {

    // 执行失败
    String FAILED = "CRM-000000";

    // 执行成功
    String SUCCESS = "CRM-000001";

    // 空字符
    String EMPTY_STRING = "CRM-000002";

    // 数据不存在
    String NO_DATA_EXIST = "CRM-000003";

    // 空
    String IS_NULL = "CRM-000004";

    // 枚举值错误
    String ENUM_ERROR = "CRM-000005";

    // 互斥
    String MUTEX = "CRM-000006";

    // 每页大小异常
    String PAGE_SIZE_ERROR = "CRM-000007";

    // 页码数异常
    String PAGE_NUM_ERROR = "CRM-000008";

    // 参数不合法
    String PARAMETER_NOT_VALID = "CRM-000009";

    // 数据不符合期望：为空或数量不等于期望的大小
    String DATA_AMOUNT_NOT_EXPECT="CRM-000010";

    // 操作不符合期望
    String ACTION_NOT_EXPECT="CRM-000011";

    // 操作类型不符合期望
    String OPFlAG_NOT_EXPECT="CRM-000012";

    // 数据已经存在
    String DATA_EXIST = "CRM-000013";

    // 数据状态不正确
    String DATA_STATUS_ERROR = "CRM-000014";

    // 文件格式不正确
    String ERROR_FILE_TYPE = "CRM-000015";

}
