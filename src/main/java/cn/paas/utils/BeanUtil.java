package cn.paas.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.UUID;

/**
 * 分转元,元转分
 * 生成32为的UUID
 */
public class BeanUtil implements Serializable{

    /**
     * 将字符串转换成Long，单位为分
     * @param val 字符串金额
     * @return long返回单位为分的整数
     */
    public static long toCurrency(String val) {
        return toCurrency(val, 2);
    }

    /**
     * 根据指定精度,将字符串转换成整数
     * @param val 字符串值
     * @param precision 精度,如保留2位则填2(分为单位),如果保留3位则填3(厘为单位)
     * @return long 整数
     */
    private static long toCurrency(String val, int precision) {
        if ((null == val) || (val.trim().length() == 0)) {
            return 0L;
        }

        val = val.replaceAll("￥", "").replaceAll(",", "").replaceAll("[$]", "");
        if ((null == val) || (val.trim().length() == 0)) {
            return 0L;
        }

        BigDecimal decimal = new BigDecimal(val);
        decimal = decimal.movePointRight(precision);

        return decimal.toBigInteger().longValue();
    }

    /**
     * 将以分为单位的金额转换成字符串
     * @param currency 以为分单位的金额
     * @return String
     */
    public static String currency2String(Long currency) {
        return currency2String(currency, 2);
    }

    /**
     * 将以指定精度的金额转换成字符串
     * @param currency 整数金额
     * @param precision 精度 2表示以分为单位　３表示以厘为单位
     * @return String
     */
    private static String currency2String(Long currency, int precision) {
        if  (null == currency) {
            currency = 0L;
        }

        final DecimalFormat decimalFormat = new DecimalFormat("0." + "000".substring(0, precision));
        BigDecimal decimal = new BigDecimal(currency);

        return decimalFormat.format(decimal.movePointLeft(precision));
    }

    /**
     * 生成32位长度的uuid字符串
     * @return String
     */
    public static String generateUUID() {
        StringBuilder fileUUID = new StringBuilder(UUID.randomUUID().toString());
        for (int i = (fileUUID.length() - 1); i >= 0; --i) {
            if (fileUUID.charAt(i) == '-') {
                fileUUID.deleteCharAt(i);
            }
        }
        return fileUUID.toString();
    }
}
