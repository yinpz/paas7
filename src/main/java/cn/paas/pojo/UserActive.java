package cn.paas.pojo;

import java.util.Date;

public class UserActive {
    private Integer avtiveId;

    private String mobile;

    private Integer count;

    private Date createTime;

    private Integer state;

    public Integer getAvtiveId() {
        return avtiveId;
    }

    public void setAvtiveId(Integer avtiveId) {
        this.avtiveId = avtiveId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}