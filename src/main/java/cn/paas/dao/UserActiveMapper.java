package cn.paas.dao;

import cn.paas.pojo.UserActive;

public interface UserActiveMapper {
    int deleteByPrimaryKey(Integer avtiveId);

    int insert(UserActive record);

    int insertSelective(UserActive record);

    UserActive selectByPrimaryKey(Integer avtiveId);

    int updateByPrimaryKeySelective(UserActive record);

    int updateByPrimaryKey(UserActive record);
}