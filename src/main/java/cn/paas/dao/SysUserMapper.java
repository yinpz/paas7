package cn.paas.dao;

import cn.paas.pojo.SysUser;

public interface SysUserMapper {
    int deleteByPrimaryKey(Integer opId);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Integer opId);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    SysUser selectByMobile(String mobile);
}