package cn.paas.service.impl;

import cn.paas.constant.CommonConst;
import cn.paas.dao.SysUserMapper;
import cn.paas.dao.UserActiveMapper;
import cn.paas.pojo.SysUser;
import cn.paas.pojo.UserActive;
import cn.paas.service.interfaces.ISysUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;


@Service("userService")
public class SysUserServiceImpl implements ISysUserService {

    @Resource
    private SysUserMapper userMapper;

    @Resource
    private UserActiveMapper activeMapper;

    public int loginUser(String mobile, String loginPass) throws Exception {
        SysUser sysUser = userMapper.selectByMobile(mobile);
        if (sysUser != null) {
            if (sysUser.getLoginPass().equals(loginPass)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public int registerUser(SysUser sysUser) throws Exception {
        sysUser.setIsAdmin(CommonConst.Admin.unisadmin);
        sysUser.setCreateTime(new Timestamp(System.currentTimeMillis()));
        sysUser.setDoneTime(new Timestamp(System.currentTimeMillis()));
        sysUser.setState(CommonConst.State.available);
        int i = 0;
        if(sysUser.getMobile() != null
                && sysUser.getLoginPass() != null) {
            i = userMapper.insertSelective(sysUser);
        } else {
            i = 0;
        }

        return i;
    }

    public int userCount(UserActive userActive) throws Exception {
        userActive.setCreateTime(new Timestamp(System.currentTimeMillis()));
        userActive.setState(CommonConst.State.available);

        return activeMapper.insert(userActive);
    }

    public int openMember(String mobile) throws Exception {

        SysUser sysUser = new SysUser();
        sysUser.setMobile(mobile);
        SysUser user = userMapper.selectByMobile(mobile);
        user.setOpId(user.getOpId());
        user.setLoginName(user.getLoginName());
        user.setLoginPass(user.getLoginPass());
        user.setMobile(user.getMobile());
        user.setEmail(user.getEmail());
        user.setState(CommonConst.State.available);
        user.setDoneTime(new Timestamp(System.currentTimeMillis()));
        user.setIsAdmin(CommonConst.Admin.isadmin);
        user.setRemarks(user.getRemarks());

        return userMapper.updateByPrimaryKeySelective(user);
    }

}
