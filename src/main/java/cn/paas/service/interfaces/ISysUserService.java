package cn.paas.service.interfaces;


import cn.paas.pojo.SysUser;
import cn.paas.pojo.UserActive;

public interface ISysUserService {

    /**
     * 校验用户名和密码
     * @param mobile    手机号
     * @param loginPass 密码
     * @return 0为错误(手机号或密码错误,手机号不存在等等),1为成功
     * @throws Exception 系统异常
     */
    int loginUser(String mobile, String loginPass) throws Exception;

    /**
     * 用户注册
     * @param sysUser 用户信息
     * @return 0为失败, 1为成功
     * @throws Exception 系统异常
     */
    int registerUser(SysUser sysUser) throws Exception;

    /**
     * 统计登录次数
     * @param userActive 入参
     * @return 0为失败,1为成功
     * @throws Exception 系统异常
     */
    int userCount(UserActive userActive) throws Exception;

    /**
     * 开通会员
     * @param mobile 入参
     * @return 0为失败,1为成功
     * @throws Exception 系统异常
     */
    int openMember(String mobile) throws Exception;

}
