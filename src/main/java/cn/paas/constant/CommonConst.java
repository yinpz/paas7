package cn.paas.constant;

/**
 * 常量定义
 */
public interface CommonConst {

    // 表记录状态
    interface State {
        int available = 1;      // 记录可用
        int unavailable = 0;    // 记录不可用
    }

    // 管理员状态
    interface Admin {
        int isadmin = 1;      // 管理员
        int unisadmin = 0;    // 普通会员
    }

    // 统计次数
    interface Count {
        int zero = 0;      // 0次
        int one = 1;       // 1ci
    }

}
